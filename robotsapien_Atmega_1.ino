#include <IRremote.h>
#include "RobosapienIrCode.h"
#include "WdRemoteCode.h"



//Based on library from : http://www.righto.com/2009/08/multi-protocol-infrared-remote-library.html
//and code from         : http://playground.arduino.cc/Main/RoboSapienIR
//and command from      : http://markcra.com/blog/?page_id=56

//#define DEBUG 1


/* Arduino*/
const int IRIn = 2;            // We will use an interrupt
const int IROut= 3;            // Where the echoed command will be sent from
const int mainLed = 13;



/*ATtiny85
const int IRIn = 0;            // We will use an interrupt
const int IROut= 1;            // Where the echoed command will be sent from
const int mainLed = 2;
*/

IRrecv irrecv(IRIn);
decode_results results;
int bitTime=516;          // Bit time (Theoretically 833 but 516)


void setup()                    
{
  pinMode(IRIn, INPUT);
  pinMode(IROut, OUTPUT);
  pinMode(mainLed,OUTPUT);
  digitalWrite(IROut,HIGH);

  #ifdef DEBUG Serial.begin(9600);
  #endif
  irrecv.enableIRIn(); // Start the receiver
}


// send the whole 8 bits
void RSSendCommand(int command) {
  digitalWrite(IROut,LOW);
  delayMicroseconds(8*bitTime);
  for (int i=0;i<8;i++) {
    digitalWrite(IROut,HIGH);  
    delayMicroseconds(bitTime);
    if ((command & 128) !=0) delayMicroseconds(3*bitTime);
    digitalWrite(IROut,LOW);
    delayMicroseconds(bitTime);
    command <<= 1;
  }
  digitalWrite(IROut,HIGH);
  delay(250); // Give a 1/4 sec before next
}

void loop() 
{ 
  if (irrecv.decode(&results)) {
    //Serial.println(results.value,HEX);
    
     #ifdef DEBUG Serial.print("Received : " );
     #endif
     #ifdef DEBUG Serial.println(results.value);
     #endif
    SwitchCommand(results.value);
    irrecv.resume(); // Receive the next value
  }
}

void SwitchCommand(long rcvIr)
{
  if(rcvIr == wdUP){
    RSSendCommand(IR_Burp);
  }
  if(rcvIr == wdPower){
    digitalWrite(mainLed,HIGH);
    delay(1000);
    digitalWrite(mainLed,LOW);
  }
}


