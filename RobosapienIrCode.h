//Movement Commands (no shift)
#define IR_TurnRight       0x80
#define IR_RightArmUp      0x81
#define IR_RightArmOut     0x82
#define IR_TiltBodyRight   0x83
#define IR_RightArmDown    0x84
#define IR_RightArmIn      0x85
#define IR_WalkForward     0x86
#define IR_WalkBackward    0x87
#define IR_TurnLeft        0x88
#define IR_LeftArmUp       0x89
#define IR_LeftArmOut      0x8A
#define IR_TiltBodyLeft    0x8B
#define IR_LeftArmDown     0x8C
#define IR_LeftArmIn       0x8D
#define IR_Stop            0x8E

//Programming Commands (no shift)
#define IR_Mcp             0x90 //P (Master Command Program)
#define IR_Play            0x91 //P>> (Program Play, the one on the bottom)
#define IR_RsensProg       0x92 //R>> (Right sensor program)
#define IR_LsensProg       0x93 //L>> (Left sensor program)
#define IR_sonicSensProg   0x94 //S>> (Sonic sensor program)

//GREEN shift commands
#define IR_RightTurnStep   0xA0 //right turn step
#define IR_LeftTurnStep    0xA1 //right hand thump
#define IR_RightHandThrow  0xA2 //right hand throw
#define IR_Sleep           0xA3 //sleep
#define IR_RightHandPickup 0xA4 //right hand pickup
#define IR_LeanBackward    0xA5 //lean backward
#define IR_ForwardStep     0xA6 //forward step
#define IR_BackwardStep    0xA7 //backward step
#define IR_LeftTurnStep    0xA8 //left turn step
#define IR_LeftHandThump   0xA9 //left hand thump
#define IR_LeftHandThrow   0xAA //left hand throw
#define IR_Listen          0xAB //listen
#define IR_LeftHandPickup  0xAC //left hand pickup
#define IR_Lean forward    0xAD //lean forward
#define IR_reset           0xAE //reset

#define IR_Execute           0xB0 //Execute (master command program execute)
#define IR_WakeUp            0xB1 //Wakeup
#define IR_RightSensorProgEx 0xB2 //Right (right sensor program execute)
#define IR_LeftSensorProgEx  0xB3 //Left (left sensor program execute)
#define IR_SonicSensorProgEx 0xB4 //Sonic (sonic sensor program execute)

//ORANGE shift commands
#define IR_RightHandStrike3  0xC0 //right hand strike 3
#define IR_RightHandSweep    0xC1 //right hand sweep
#define IR_Burp              0xC2 //burp
#define IR_RightHandStrike2  0xC3 //right hand strike 2
#define IR_High5             0xC4 //high 5
#define IR_RightHandStrike1  0xC5 //right hand strike 1
#define IR_Bulldozer         0xC6 //bulldozer
#define IR_Fart              0xC7 //oops (fart)
#define IR_LeftHandStrike3   0xC8 //left hand strike 3
#define IR_LeftHandSweep     0xC9 //left hand sweep
#define IR_Whistle           0xCA //whistle
#define IR_LeftHandStrike2   0xCB //left hand strike 2
#define IR_TalkBack          0xCC //talkback
#define IR_LeftHandStrike1   0xCD //left hand strike 1
#define IR_Roar              0xCE //roar


#define IR_AllDemo           0xD0 //All Demo
#define IR_PowerOff          0xD1 //Power Off (drop snow-globe and say “Rosebud”)
#define IR_Demo1             0xD2 //Demo 1 (Karate skits)
#define IR_Demo2             0xD3 //Demo 2 (Rude skits)
#define IR_Dance             0xD4 //Dance

